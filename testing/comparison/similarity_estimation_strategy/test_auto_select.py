from typing import Optional

from comparison.similarity_estimation_strategy.auto_select import auto_select
from comparison.similarity_estimation_strategy.least_square import LeastSquareStrategy


def test_happy_path():
    """
    Ensure that a Strategy can automatically be selected by the name.
    """
    assert auto_select("LeastSquareStrategy").__class__ == LeastSquareStrategy


def test_invalid_extension():
    """
    Ensure correct Exception Behavior.
    """
    exception: Optional[Exception] = None
    try:
        _ = auto_select("this name does not exist")
    except Exception as e:
        exception = e

    assert exception.__class__ == ValueError
