from pandas import DataFrame
from comparison.similarity_estimation_strategy.least_square import LeastSquareStrategy


def test_plausibility():
    """
    Runs the strategy with a Dataset with know outcome.
    """
    dataframe: DataFrame = DataFrame(
        {"x": [1, 2, 3], "y": [3, 1, 1], "z": [1000, 1000, 1000]}
    )
    error: float = LeastSquareStrategy().estimate_similarity(dataframe, "x", "y")
    assert error == 3.0, "The mean squared error of x and y must be 3"
