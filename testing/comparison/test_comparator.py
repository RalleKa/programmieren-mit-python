import pandas as pd

from comparison.comparator import Comparator
from comparison.similarity_estimation_strategy.least_square import (
    LeastSquareStrategy,
    AbstractSimilarityEstimationStrategy,
)


def test_retrieve_best_ideal_function():
    """
    Ensures that the happy path of the method works properly and returns the expected results.
    """
    ideal_functions: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y1": [3, 1, 1], "y2": [1, 1, 3]}
    ).set_index("x")
    training_data: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y": [1, 2, 3]}
    ).set_index("x")
    strategy: AbstractSimilarityEstimationStrategy = LeastSquareStrategy()
    comparator: Comparator = Comparator(
        ideal_functions=ideal_functions,
        similarity_estimation_strategy=strategy,
    )

    error: pd.DataFrame = comparator.retrieve_best_ideal_function(training_data, "y")

    assert error.index[0] == "y"
    assert error.iloc[0]["ideal_function"] == "y2"
    assert error.iloc[0]["similarity"] == 1 / 3
    assert error.iloc[0]["similarity_strategy"] == strategy.get_name()


def test_retrieve_best_ideal_function_illegal_dataframe_format():
    """
    Ensures that the method can not be called with wrong parameters.
    """
    ideal_functions: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y1": [3, 1, 1], "y2": [1, 1, 3]}
    ).set_index("x")
    training_data: pd.DataFrame = pd.DataFrame(
        {"x": [1, 3, 2, 4], "y": [1, 3, 2, 4]}
    ).set_index("x")
    strategy: AbstractSimilarityEstimationStrategy = LeastSquareStrategy()
    comparator: Comparator = Comparator(
        ideal_functions=ideal_functions,
        similarity_estimation_strategy=strategy,
    )

    error: pd.DataFrame = comparator.retrieve_best_ideal_function(training_data, "y")

    assert error.index[0] == "y"
    assert error.iloc[0]["ideal_function"] == "y2"
    assert error.iloc[0]["similarity"] == 1 / 3
    assert error.iloc[0]["similarity_strategy"] == strategy.get_name()


def test_get_ideal_function():
    """
    Ensures that the happy path of the method works properly and returns the expected results.
    """
    ideal_functions: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y1": [3, 1, 1], "y2": [1, 1, 3]}
    ).set_index("x")
    training_data: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y1": [1, 2, 3], "y2": [3, 2, 1]}
    ).set_index("x")
    strategy: AbstractSimilarityEstimationStrategy = LeastSquareStrategy()
    comparator: Comparator = Comparator(
        ideal_functions=ideal_functions,
        similarity_estimation_strategy=strategy,
    )

    error: pd.DataFrame = comparator.get_ideal_function(training_data)

    assert error.index[0] == "y1"
    assert error.iloc[0]["ideal_function"] == "y2"
    assert error.iloc[0]["similarity"] == 1 / 3
    assert error.iloc[0]["similarity_strategy"] == strategy.get_name()

    assert error.index[1] == "y2"
    assert error.iloc[1]["ideal_function"] == "y1"
    assert error.iloc[1]["similarity"] == 1 / 3
    assert error.iloc[1]["similarity_strategy"] == strategy.get_name()


def test_get_ideal_function_illegal_dataframe_format():
    """
    Ensures that the method can not be called with wrong parameters.
    """
    ideal_functions: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y1": [3, 1, 1], "y2": [1, 1, 3]}
    ).set_index("x")
    training_data: pd.DataFrame = pd.DataFrame(
        {"x": [1, 3, 2, 4], "y1": [1, 3, 2, 4], "y2": [3, 1, 2, 4]}
    ).set_index("x")
    strategy: AbstractSimilarityEstimationStrategy = LeastSquareStrategy()
    comparator: Comparator = Comparator(
        ideal_functions=ideal_functions,
        similarity_estimation_strategy=strategy,
    )

    error: pd.DataFrame = comparator.get_ideal_function(training_data)

    assert error.index[0] == "y1"
    assert error.iloc[0]["ideal_function"] == "y2"
    assert error.iloc[0]["similarity"] == 1 / 3
    assert error.iloc[0]["similarity_strategy"] == strategy.get_name()

    assert error.index[1] == "y2"
    assert error.iloc[1]["ideal_function"] == "y1"
    assert error.iloc[1]["similarity"] == 1 / 3
    assert error.iloc[1]["similarity_strategy"] == strategy.get_name()


def test_get_ideal_function_per_row():
    """
    Ensures that the happy path of the method works properly and returns the expected results.
    """
    ideal_functions: pd.DataFrame = pd.DataFrame(
        {"x": [1, 2, 3], "y1": [3, 1, 1], "y2": [1, 1, 3]}
    ).set_index("x")
    test_data: pd.DataFrame = pd.DataFrame({"x": [1, 3], "y": [1, 1]}).set_index("x")
    strategy: AbstractSimilarityEstimationStrategy = LeastSquareStrategy()
    comparator: Comparator = Comparator(
        ideal_functions=ideal_functions,
        similarity_estimation_strategy=strategy,
    )

    error: pd.DataFrame = comparator.get_ideal_function_per_row(test_data)

    assert error.index.name == "x"
    assert list(error.columns.values) == [
        "y",
        "ideal_function",
        "similarity",
        "similarity_strategy",
    ]

    assert error.index[0] == 1
    assert error.iloc[0]["y"] == 1
    assert error.iloc[0]["ideal_function"] == "y2"
    assert error.iloc[0]["similarity"] == 0
    assert error.iloc[0]["similarity_strategy"] == strategy.get_name()

    assert error.index[1] == 3
    assert error.iloc[1]["y"] == 1
    assert error.iloc[1]["ideal_function"] == "y1"
    assert error.iloc[1]["similarity"] == 0
    assert error.iloc[1]["similarity_strategy"] == strategy.get_name()
