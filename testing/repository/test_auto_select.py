from typing import Optional

from repository.auto_select import auto_select
from repository.csv_repository import CSVRepository


def test_happy_path():
    """
    Ensure that a Repository can automatically be selected by the path-name.
    """
    assert auto_select("path.csv", "x", ",").__class__ == CSVRepository


def test_invalid_extension():
    """
    Ensure correct Exception Behavior.
    """
    exception: Optional[Exception] = None
    try:
        _ = auto_select("this extension does not exist", "x", ",")
    except Exception as e:
        exception = e

    assert exception.__class__ == IOError
