from typing import Optional
from os import path
from pathlib import Path
import pandas as pd

from repository.abstract_repository import AbstractRepository
from repository.csv_repository import CSVRepository


def test_load_happy_path():
    """
    Tests if a csv dataset can be loaded when the correct parameters are given.
    """
    repo: AbstractRepository = CSVRepository(
        Path(path.join("datasets", "test.csv")), "x", ","
    )
    dataframe: pd.Dataframe = repo.load()

    assert len(dataframe) == 100, "The dataset must contain 100 rows"
    assert len(dataframe["y"]) == 100, "The y-column must contain 100 rows"


def test_load_wrong_delimiter():
    """
    Ensures the correct behavior, when the wrong delimiter is passed to the Object.
    """
    exception: Optional[Exception] = None
    try:
        repo: AbstractRepository = CSVRepository(
            Path(path.join("datasets", "test.csv")), "x", "\t"
        )
        dataframe: pd.Dataframe = repo.load()
    except Exception as e:
        exception = e

    # as the wrong delimiter is used, the dataset has an incorrect index_name and therefore raises a KeyError
    assert exception.__class__ == KeyError


def test_load_wrong_path():
    """
    Ensures a connection when the path points to a non-existent file.
    """
    repo: AbstractRepository = CSVRepository(
        Path(path.join("datasets", "no_such_file.csv")), "x", ","
    )

    exception: Optional[Exception] = None
    try:
        repo.load()
    except FileNotFoundError as e:
        exception = e

    assert (
        exception is not None
    ), "The csv file does not exist. Thus, it should not be loaded"


def test_save_happy():
    """
    Ensures that the data can be written to a file (no matter if it already exists).
    Uses the load-functionality to ensure, that the data was stored correctly.
    """
    repo: AbstractRepository = CSVRepository(
        Path(path.join("datasets", "test_output.csv")), "col1", ","
    )

    # saving the data
    dataframe: pd.Dataframe = pd.DataFrame({"col1": [1, 2], "col2": [2, 4]})
    repo.save(data=dataframe)

    # loading and checking the data
    dataframe: pd.Dataframe = repo.load()
    assert (
        len(dataframe) == 2
    ), "when loading the data back in again, it must be identical to the persisted data"
    assert (
        len(dataframe["col2"]) == 2
    ), "when loading the data back in again, it must be identical to the persisted data"
