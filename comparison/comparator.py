from typing import List

from controller import logger
import pandas as pd

from comparison.similarity_estimation_strategy.abstract_similarity_estimation_strategy import (
    AbstractSimilarityEstimationStrategy,
)


ideal_prefix: str = "ideal_"


class Comparator:
    """
    Compares DataSets to IdealFunctions. Returns the best fitting ideal_function for each input dataset.
    Using a Strategy-pattern the similarity-estimation-method can be customized.
    """

    def __init__(
        self,
        ideal_functions: pd.DataFrame,
        similarity_estimation_strategy: AbstractSimilarityEstimationStrategy,
    ):
        """
        Setting up the Comparator.
        :param ideal_functions: A Dataset containing the ideal_functions and the independent_variable
        :param similarity_estimation_strategy: The strategy for comparing similarities of two datasets
        :raises
            AssertionError: When the given data is incorrect
        """
        # storing relevant data
        self.log = logger.get_logger(__name__)
        self.ideal_functions: pd.DataFrame = ideal_functions.add_prefix(ideal_prefix)
        self.similarity_estimation_strategy: AbstractSimilarityEstimationStrategy = (
            similarity_estimation_strategy
        )

    def __calculate_similarities(
        self, dataset: pd.DataFrame, dependant_variable: str
    ) -> List[float]:
        """
        Calculates the similarity of a single Dataset to every ideal_function.
        :param dataset: The DataFrame containing the Dataset
        :param dependant_variable: The column-name of the Dataset
        :return:
        """
        estimated_similarities: List[float] = [
            self.similarity_estimation_strategy.estimate_similarity(
                dataset, dependant_variable, function_header
            )
            for function_header in self.ideal_functions.columns
        ]
        self.log.debug(
            f"Similarities for dependant_variable {dependant_variable} are {estimated_similarities}"
        )
        return estimated_similarities

    def retrieve_best_ideal_function(
        self, dataset: pd.DataFrame, dependant_variable: str
    ) -> pd.DataFrame:
        """
        Finds the best fitting ideal function for a Dataset represented by a column of a DataFrame.
        :param dataset: The DataFrame containing the Dataset
        :param dependant_variable: The column-name of the Dataset in the DataFrame
        :return: A DataFrame containing useful information. It will always have the following columns:
            {"input_column_name", "ideal_column_name", "similarity", "similarity_strategy"}
        """
        combined_dataset: pd.DataFrame = (
            pd.concat([dataset.stack(), self.ideal_functions.stack()], axis=0)
            .unstack()
            .dropna()
        )
        assert len(combined_dataset) != 0, "both datasets must share indices"

        # get the index of the most equal ideal function
        similarity_estimation: List[float] = self.__calculate_similarities(
            combined_dataset, dependant_variable
        )
        index: int = similarity_estimation.index(min(similarity_estimation))

        # get the column-name of that ideal function
        column_name: str = self.ideal_functions.columns[index][len(ideal_prefix) :]
        self.log.info(
            f"The ideal_function {column_name} with the index {index} is the best "
            f"fit for dependant_variable {dependant_variable}"
        )
        result: pd.DataFrame = pd.DataFrame(
            {
                "ideal_function": column_name,
                "similarity": similarity_estimation[index],
                "similarity_strategy": self.similarity_estimation_strategy.get_name(),
            },
            [dependant_variable],
        )
        result.index.name = "input_function"
        return result

    def get_ideal_function(self, dataset: pd.DataFrame) -> pd.DataFrame:
        """
        Calculates the best ideal_function for each input dataset in the DataFrame.
        :param dataset: The DataFrame containing all the input datasets
        :return: A new DataFrame containing information on the assignment:
            {"input_column_name", "ideal_column_name", "similarity", "similarity_strategy"}
        """
        self.log.debug(
            f"Best ideal_function for the dataset using the {self.similarity_estimation_strategy.get_name()}"
        )
        result: List[pd.DataFrame] = [
            self.retrieve_best_ideal_function(dataset, column)
            for column in dataset.columns
        ]
        return pd.concat(result)

    def get_ideal_function_per_row(self, dataset: pd.DataFrame) -> pd.DataFrame:
        """
        Estimates the best ideal function for each Dataset.
        Therefor iterates over each row of the dataset and calculates similarities.
        :param dataset: The dataset with a couple of datapoints.
            As the dataset does not represent a function, independent variables can be included multiple times.
        :return: A new DataFrame containing information on the assignment of each row:
            {independent_variable, dependent_variable, "ideal_function", "similarity", "similarity_strategy"}
        """
        assert (
            len(dataset.columns) == 1
        ), "only 2 columns expected. Independent-variable as index and dependent-variable as column"
        column_name: str = dataset.columns[0]
        index_name: str = dataset.index.name
        dataset: pd.DataFrame = dataset.reset_index()

        rows: List[pd.DataFrame] = [
            pd.DataFrame(dataset.iloc[i]).transpose().set_index(index_name)
            for i in range(len(dataset))
        ]

        row_results: List[pd.DataFrame] = [
            self.retrieve_best_ideal_function(row, column_name) for row in rows
        ]

        results: pd.DataFrame = pd.concat(row_results).reset_index(drop=True)

        return pd.concat([dataset, results], axis=1).set_index(index_name)
