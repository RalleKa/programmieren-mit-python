from abc import ABC

import pandas as pd


class AbstractSimilarityEstimationStrategy(ABC):
    """
    Any child of this class establishes a strategy to estimate the similarity of two datasets.
    """

    def get_name(self):
        """
        A name for the strategy. For logging purposes.
        :return: A unique name to each Strategy
        """
        return self.__class__.__name__

    def estimate_similarity(
        self,
        dataframe: pd.DataFrame,
        dependent_variable1: str,
        dependent_variable2: str,
    ) -> float:
        """
        Calculates the similarity between two datasets. dataframe holds both datasets.
        The dependent_variables can not have the same names. For 2 identical datasets this function will return 0.
        Different Strategies may not be comparable, however, the same strategy should return a comparable value.
        :param dataframe: The DataFrame in which the first Dataset is stored
        :param dependent_variable1: The column-name of the first dataset in the DataFrame
        :param dependent_variable2: The column-name of the second dataset in the DataFrame
        :return: A value representing the similarity. The smaller, the more similar, with 0 being identical
        """
        raise NotImplementedError("Not yet implemented")
