from typing import List

from comparison.similarity_estimation_strategy.abstract_similarity_estimation_strategy import (
    AbstractSimilarityEstimationStrategy,
)
from comparison.similarity_estimation_strategy.least_square import LeastSquareStrategy


available_strategies: List[AbstractSimilarityEstimationStrategy] = [
    LeastSquareStrategy()
]


def auto_select(name: str) -> AbstractSimilarityEstimationStrategy:
    """
    Automatically selects the strategy based on the name.
    :param name: The name of the strategy
    :return: The SimilarityEstimationStrategy
    """
    for strategy in available_strategies:
        if strategy.get_name() == name:
            return strategy

    raise ValueError(
        f"No strategy with the name {name} was found. Available strategies are: "
        f"{[strategy.get_name() for strategy in available_strategies]}"
    )
