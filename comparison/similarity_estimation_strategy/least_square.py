import pandas as pd

from comparison.similarity_estimation_strategy.abstract_similarity_estimation_strategy import (
    AbstractSimilarityEstimationStrategy,
)
from sklearn.metrics import mean_squared_error


class LeastSquareStrategy(AbstractSimilarityEstimationStrategy):
    """
    Implementation of LeastSquareStrategy using performant methods of sklearn.metrics.
    """

    def estimate_similarity(
        self,
        dataframe: pd.DataFrame,
        dependent_variable1: str,
        dependent_variable2: str,
    ) -> float:
        """
        The mean squared error.
        :param dataframe: The DataFrame in which the first Dataset is stored
        :param dependent_variable1: The column-name of the first dataset in the DataFrame
        :param dependent_variable2: The column-name of the second dataset in the DataFrame
        :return: A value representing the similarity. The smaller, the more similar, with 0 being identical
        """
        return mean_squared_error(
            dataframe[dependent_variable1], dataframe[dependent_variable2]
        )
