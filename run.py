from typing import List

import pandas as pd
from matplotlib import pyplot as plt

from comparison.comparator import Comparator
from repository.abstract_repository import AbstractRepository
from controller.visualization import Visualizer
from repository.auto_select import auto_select as select_repo
from comparison.similarity_estimation_strategy.auto_select import (
    auto_select as select_strategy,
)
from controller.arg_parser import args
from comparison.similarity_estimation_strategy.abstract_similarity_estimation_strategy import (
    AbstractSimilarityEstimationStrategy,
)

# loading data from files
training_data: pd.DataFrame = select_repo(
    args["training_input"],
    args["training_independent_varname"],
    args["training_input_argument"],
).load()
test_data: pd.DataFrame = select_repo(
    args["test_input"], args["test_independent_varname"], args["test_input_argument"]
).load()
ideal_functions: pd.DataFrame = select_repo(
    args["ideal_input"], args["ideal_independent_varname"], args["ideal_input_argument"]
).load()

# configuration
strategy: AbstractSimilarityEstimationStrategy = select_strategy(args["strategy"])
comparator: Comparator = Comparator(ideal_functions, strategy)

# select ideal functions
pre_selection: pd.DataFrame = comparator.get_ideal_function(training_data)
preselected_ideal_functions: pd.DataFrame = ideal_functions.filter(
    pre_selection["ideal_function"].values
)

# visualize result
visualizer: Visualizer = Visualizer([training_data, ideal_functions])
for i in range(len(pre_selection)):
    # create the plot
    input_column_name: str = pre_selection.index.values[i]
    ideal_column_name: str = pre_selection["ideal_function"].values[i]
    plot: plt = visualizer.visualize_graph([input_column_name, ideal_column_name])

    # save the plot
    plot.savefig(
        args["output_image"]
        .replace("<in>", input_column_name)
        .replace("<ideal>", ideal_column_name)
    )

# use test dataset
similarity_discrepancy = float(args["similarity_discrepancy"])
comparator: Comparator = Comparator(preselected_ideal_functions, strategy)
result: pd.DataFrame = comparator.get_ideal_function_per_row(test_data)
filtered_result: pd.DataFrame = result[result["similarity"] < similarity_discrepancy]

# save results
for metadata in [
    "training",
    "ideal",
    "test",
    "result",
]:
    output_repo: AbstractRepository = select_repo(
        args[f"{metadata}_output"],
        args[f"{metadata}_independent_varname"],
        args[f"{metadata}_output_argument"],
    )
    output_repo.save(filtered_result)

# visualize result
datasets: List[pd.DataFrame] = [
    preselected_ideal_functions for i in range(len(pre_selection))
]
result.loc[result.similarity > similarity_discrepancy, "ideal_function"] = "None"
visualizer: Visualizer = Visualizer(datasets)
visualizer.visualize_graph(pre_selection["ideal_function"], result).savefig(
    args["output_image"].replace("<in>", "test").replace("<ideal>", "preselected")
)
