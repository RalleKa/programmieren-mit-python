# Programmieren mit Python

This project represents my term paper for "Programming with Python"
at the "International University of Applied Science" (IU).

## Description

TODO: Add description of the functionality of the program

## Getting started

### Installation

1. Create a local copy of the config file
   ```
   cp config.ini config-local.ini
   ```
2. Make sure you have [python](https://www.python.org/downloads/release/python-388/) installed in the correct Version.
   All allowed versions can be taken from the [`pyproject.toml` file](./pyproject.toml).
3. Make sure you have [poetry](https://python-poetry.org/docs/#installation) installed on your machine.
4. Install all packages:
   ```
   poetry install
   ```

### Usage

1. enable poetry
   ```
   poetry shell
   ```
2. Run the main program or any script on its own
   ```
   python run.py
   ```

### Docker

1. make sure you have [docker](https://docs.docker.com/get-docker/) installed on your machine
2. build the image
   ```
   docker build -t hausarbeit .
   ```
3. run the container
   ```
   docker run hausarbeit
   ```

## Contributing

Since this is my term paper, no other people can contribute to this project.
However, as described in the license, I am making my code available to other people.
Therefore here is a representation of the correct extension.

### adding new packages

```
poetry add [packgename]
```

### Program structure

![Program structure](architecture.png)

### adding functionality or fixing bugs

1. use a branch with the name feature/[description] or bug/[description]
2. make sure pre-commit is set up on your machine to verify code quality
3. commit your code and push it to origin
4. make a merge request.
   The build pipeline will verify the code by running all unit-tests.
   Therefore, it is important to write unit tests to make sure that the code works properly.

## Authors and acknowledgment

The only author of this project is and will remain Ralf Kagelmann.

## License

**MIT:**

*Copyright 2022 Ralf Kagelmann*

*Permission is hereby granted, free of charge,
to any person obtaining a copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of the Software,
and to permit persons to whom the Software is furnished to do so, subject to the following conditions:*

*The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.*

*THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*

**In Addition to the above:**

Each user of this code is encouraged to independently verify that the use of this project is permitted for personal
circumstances. Example:
- I want to use this code as a template for my own submission → Not allowed
- I would like to use this code as a template for a private project or use it in my company after thorough review and
consultation with my managers → Allowed
