FROM python:3.8

## installing system requirements
RUN apt update -y && \
    apt upgrade -y && \
    pip install poetry

## installing packages
WORKDIR /app
COPY pyproject.toml /app/
RUN poetry install --no-dev --no-interaction --no-ansi

## running the program
COPY . /app
CMD cp config.ini config-local.ini && \
    poetry run python run.py
