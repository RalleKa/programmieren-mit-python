from diagrams import Diagram, Cluster, Edge
from diagrams.aws.blockchain import QuantumLedgerDatabaseQldb
from diagrams.aws.compute import ComputeOptimizer
from diagrams.aws.network import ElasticLoadBalancing
from diagrams.aws.management import SystemsManagerRunCommand
from diagrams.aws.compute import EC2
from diagrams.aws.business import AlexaForBusiness
from diagrams.aws.compute import EC2ImageBuilder

with Diagram("Architecture", show=False):
    # repos
    with Cluster("Repositories"):
        abstract_repository = ElasticLoadBalancing("repository")
        csv_repository = QuantumLedgerDatabaseQldb("csv")
        sqlite_repository = QuantumLedgerDatabaseQldb("sqlite")
    sqlite_repository - abstract_repository - csv_repository

    # similarity
    with Cluster("Similarity Strategies"):
        abstract_similarity_estimation_strategy = ComputeOptimizer("strategy")
        least_square = EC2("least square")
    abstract_similarity_estimation_strategy - least_square

    # visualisation
    visualisation = AlexaForBusiness("visualisation")

    # comparison
    comparator = EC2("comparator")
    run = SystemsManagerRunCommand("run")

    # controller
    controller = EC2ImageBuilder("controller")
    controller >> Edge(label="configures") >> run

    # run
    run >> Edge(label="output") >> abstract_repository
    abstract_repository >> Edge(label="input") >> run

    comparator >> Edge(label="uses") >> abstract_similarity_estimation_strategy

    run >> Edge(label="manages") >> comparator
    run >> visualisation
