import pathlib
from typing import Optional, List

import pandas as pd

import sqlalchemy as db
from sqlalchemy.future import Engine

from repository.abstract_repository import AbstractRepository


class SQLiteRepository(AbstractRepository):
    """
    Read and write data from/to sqlite databases.
    The Data will be read when calling the load-method. There is no caching. So make sure you reuse the dataframe.
    """

    def __init__(self, filename: pathlib.Path, index_name: str, table_name: str):
        """
        Initializes the required parameters. Does not yet create a connection.
        :param filename: The filename of the sqlite database
        :param index_name The name of the index column
        :param table_name: The table in which the data should be stored
        """
        assert (
            table_name is not None
        ), "When using SQLiteRepository, a table_name must be set"
        super().__init__(index_name=index_name)
        self.connection_string: str = f"sqlite:///{filename}"
        self.table_name = table_name

        # create the engine only when
        self.engine: Optional[Engine] = None

    def __get_engine(self) -> Engine:
        """
        Creates a new engine if none exists yet. Uses the old engine otherwise.
        :return: An engine that can read/write to the database
        """
        if self.engine is None:
            self.log.debug(
                f"creating connection to sqlite-database using the connection-string {self.connection_string}"
            )
            self.engine = db.create_engine(self.connection_string)
        return self.engine

    def _load(self) -> pd.DataFrame:
        """
        Load Data from the SQLite Database.
        :return: A DataFrame containing the Data
        :raises
            FileNotFoundError: When the file does not exist
        """
        try:
            return pd.read_sql(f"select * from {self.table_name}", self.__get_engine())
        except Exception as e:
            raise FileNotFoundError(e)

    def _save(self, data: pd.DataFrame) -> None:
        """
        Saves data at the Database. It drops the old Table.
        That way can be ensured, that the Table only contains the data from the dataframe.
        When you'd rather want to append the Data, try loading the current data and connect the two dataframes.
        We do not need a schema, as Pandas analyzes the data-types for us. (see `pandas.DataFrame.info()`)
        :param data: The data that must be written
        """
        data.to_sql(self.table_name, con=self.__get_engine(), if_exists="replace")

    @staticmethod
    def extension() -> List[str]:
        """
        Returns a list of possible file-extensions.
        :return: a list of possible file-extensions
        """
        return ["db"]
