import pathlib
from typing import Optional

from repository.abstract_repository import AbstractRepository
from repository.csv_repository import CSVRepository
from repository.sqlite_repository import SQLiteRepository


available_repo_types = [CSVRepository, SQLiteRepository]


def auto_select(
    path: str, index_name: str, argument: Optional[str]
) -> AbstractRepository:
    """
    Automatically selects a repository type based on the extension of the path.
    :param path: The path to the data sink
    :param index_name The name of the index column
    :param argument: An additional argument that might be required to initialize the connection to the file
    :return: A connection to the given file
    """
    extension: str = path.split(".")[-1]

    for repo_type in available_repo_types:
        if extension in repo_type.extension():
            return repo_type(pathlib.Path(path), index_name, argument)

    raise IOError(
        f"File type with extension {extension} not allowed. Only "
        f"{[extension for repo_type in available_repo_types for extension in repo_type.extension() ]}"
    )
