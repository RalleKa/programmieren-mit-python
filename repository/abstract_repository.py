from abc import ABC
from typing import List

from controller import logger
import pandas as pd


NOT_IMPLEMENTED: str = "Not yet implemented"


class AbstractRepository(ABC):
    """
    An abstract object that allows to read or write data from or to a datasource.
    """

    def __init__(self, index_name: str):
        """
        Initialize the parent.
        :param index_name The name of the index column
        """
        self.log = logger.get_logger(__name__)
        self.index_name = index_name

    def _load(self) -> pd.DataFrame:
        """
        Load Data from the source. Do not set index yet.
        :return: A DataFrame containing the Data
        :raises
            FileNotFoundError: When the file does not exist
            KeyError: When the data is not formatted correctly
        """
        raise NotImplementedError(NOT_IMPLEMENTED)

    def load(self) -> pd.DataFrame:
        """
        Load Data from the source.
        :return: A DataFrame containing the Data
        :raises
            FileNotFoundError: When the file does not exist
            KeyError: When the data is not formatted correctly
        """
        return self._load().set_index(self.index_name)

    def _save(self, data: pd.DataFrame) -> None:
        """
        Saves data to a Data sink.
        :param data: The data that must be written
        """
        raise NotImplementedError(NOT_IMPLEMENTED)

    def save(self, data: pd.DataFrame) -> None:
        """
        Saves data to a Data sink. Therefor unpacks the index.
        :param data: The data that must be written
        """
        self._save(data)

    @staticmethod
    def extension() -> List[str]:
        """
        Returns a list of possible file-extensions.
        :return: a list of possible file-extensions
        """
        raise NotImplementedError(NOT_IMPLEMENTED)
