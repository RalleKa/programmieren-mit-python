import pathlib
from typing import List, Optional

import pandas as pd

from repository.abstract_repository import AbstractRepository


class CSVRepository(AbstractRepository):
    """
    Read and write data from/to csv-files.
    The Data will be read when calling the load-method. There is no caching. So make sure you reuse the dataframe.
    """

    def __init__(
        self, filename: pathlib.Path, index_name: str, delimiter: Optional[str]
    ):
        """
        Set necessary parameters. A new Dataset always begins with a new Line.
        :param filename: The path to the csv file. Any absolute or relative path is allowed
        :param index_name The name of the index column
        :param delimiter: How the columns are separated, default is ","
        """
        super().__init__(index_name=index_name)
        self.filename: pathlib.Path = filename
        self.delimiter: str = delimiter if delimiter is not None else ","

    def _load(self) -> pd.DataFrame:
        """
        Load Data from a csv file. There is no caching. So make sure you reuse the dataframe.
        :return: A pandas Dataframe containing the Dataset
        :raises
            FileNotFoundError: When the file does not exist
            KeyError: When the data is not formatted correctly
        """
        self.log.debug(
            f"loading Dataframe from {self.filename} with delimiter {self.delimiter}"
        )
        return pd.read_csv(self.filename, delimiter=self.delimiter)

    def _save(self, data: pd.DataFrame) -> None:
        """
        Writes the data to the File. It will not be checked if the Data already contains the exact same data.
        :param data: The Data that should be written to the csv file
        """
        self.log.debug(
            f"writing Dataframe to {self.filename} with delimiter {self.delimiter}"
        )
        data.to_csv(self.filename, sep=self.delimiter)

    @staticmethod
    def extension() -> List[str]:
        """
        Returns a list of possible file-extensions.
        :return: a list of possible file-extensions
        """
        return ["csv"]
