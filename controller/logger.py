import logging
from controller.arg_parser import args


log_level: logging = logging.DEBUG if args["verbose"] else logging.INFO
logging.basicConfig(filename=args["output_logging"], level=log_level)


def get_logger(name):
    """
    Creates a logger with the default logging config.
    :param name: The name of the logger
    :return: a new logging object
    """
    return logging.getLogger(name)
