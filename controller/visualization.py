from itertools import cycle, islice
from typing import List, Generator, Optional

import matplotlib.pyplot as plt
import pandas as pd
from pandas.plotting._matplotlib.style import Color
import numpy as np


class Visualizer:
    """
    Visualizes data in various predefined ways.
    """

    def __init__(self, datasets: List[pd.DataFrame]):
        """
        Initializes the object.
        :param datasets: a Dataframe for each column to be visualized
        """
        self.datasets: List[pd.DataFrame] = datasets

    def __link(self, columns: List[str]) -> Generator[pd.DataFrame, None, None]:
        """
        Links the datasets and column names.
        :param columns: the name of the column
        :return: A generator. Each element is a dataset containing only of the linked column
        """
        assert len(self.datasets) == len(
            columns
        ), "Must have a column for each dataset and a dataset for each column"

        for i in range(len(columns)):
            column_reformatted: str = f"var{i}_{columns[i]}"
            extracted_dataset: pd.DataFrame = self.datasets[i][[columns[i]]].rename(
                columns={columns[i]: column_reformatted}
            )
            yield extracted_dataset

    def visualize_graph(
        self, columns: List[str], points: Optional[pd.DataFrame] = None
    ) -> plt:
        """
        Creates a linegraph (plot) for the dependent variables given in the columns-list.
        Must have a column for each dataset and a dataset for each column. They get linked by index.
        :param columns: The dependent variables
        :param points: Some additional points to be added to the plot
        :return: The linegraph (plot) as matplotlib.pyplot
        """
        # create a list of colors for the plot. We need one spare color for datapoints with no ideal function
        colors: List[Color] = list(
            islice(cycle(["b", "r", "g", "y", "k"]), None, len(columns) + 1)
        )

        # create a DataFrame with the column that should be drawn
        dataset: pd.DataFrame = (
            pd.concat([col.stack() for col in self.__link(columns)], axis=0)
            .unstack()
            .dropna()
        )

        # create the plot
        plot: plt = dataset.plot(color=colors)

        # add points if required
        if points is not None:
            # finding the independent_variable and dependent_variable of the datapoints
            independent_variable: str = points.index.name
            dependent_variable: str = [
                column
                for column in points.columns.values
                if column
                not in [
                    independent_variable,
                    "ideal_function",
                    "similarity",
                    "similarity_strategy",
                ]
            ][0]
            # reset the index, so we can access each row even if the independent_variable contains duplicates
            points = points.reset_index()

            # retrieve the index of the ideal function from the two DataFrames
            ideal: List[List[int]] = [
                np.where(columns == ideal_function)[0]
                for ideal_function in points["ideal_function"].values
            ]
            # retrieve the color of the ideal function.
            # When the ideal function was not found in the datasets (there is no ideal function for the datapoint),
            # we select an unused color.
            color_map: List[Color] = [
                colors[index[0] if len(index) == 1 else -1] for index in ideal
            ]

            plot.scatter(
                points[independent_variable].values,
                points[dependent_variable].values,
                color=color_map,
            )

        # draw the plot
        return plot.get_figure()
