import configparser
import sys
from typing import List

# try to find a config parameter manually
# argparse can not yet be used, as the defaults must be configured by the config
path: str = "config-local.ini"
mask: List[bool] = ["--config" in sys.argv, "-c" in sys.argv]
if True in mask:
    if mask[0]:
        path = sys.argv[sys.argv.index("--config") + 1]
    else:
        path = sys.argv[sys.argv.index("-c") + 1]

config = configparser.ConfigParser()
config.read(path)
