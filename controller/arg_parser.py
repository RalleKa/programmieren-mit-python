import argparse
from controller.config import config

parser = argparse.ArgumentParser(
    description="find the best ideal function for each input"
)

# training, test, ideal and result share mostly the same format. Using a loop to initialize them
for data_type in ["training", "test", "ideal", "result"]:
    section: str = data_type.upper() if data_type != "result" else "OUTPUT"

    if data_type != "result":
        parser.add_argument(
            f"--{data_type}-input",
            default=config[section]["input_path"],
            help=f"path to the input file of {data_type} data",
        )
        parser.add_argument(
            f"--{data_type}-input-argument",
            default=config[section]["input_argument"],
            help="additional argument for the connection to the repository",
        )
        parser.add_argument(
            f"--{data_type}-independent-varname",
            default=config[section]["independent_var_name"],
            help="The name of the independent variable in the dataset",
        )
    else:
        parser.add_argument(
            "-l",
            "--output-logging",
            default=config["OUTPUT"]["logging_path"],
            help="Output for logging",
        )
        parser.add_argument(
            "-i",
            "--output-image",
            default=config["OUTPUT"]["image_path"],
            help="path to the output file for images. May contain <in> or <ideal> as placeholders",
        )

    parser.add_argument(
        f"--{data_type}-output",
        default=config[section]["output_path"],
        help=f"path to the output file for {data_type} data",
    )
    parser.add_argument(
        f"--{data_type}-output-argument",
        default=config[section]["output_argument"],
        help="additional argument for the connection to the repository",
    )


# setup general
parser.add_argument(
    "-s",
    "--strategy",
    default=config["GENERAL"]["strategy"],
    help="The strategy to estimate similarity",
)
parser.add_argument(
    "-d",
    "--similarity-discrepancy",
    default=config["GENERAL"]["similarity_discrepancy"],
    help="the max discrepancy between a datapoint and an ideal function",
)
parser.add_argument(
    "-v", "--verbose", help="increase output verbosity", action="store_true"
)
# adding config
parser.add_argument(
    "-c", "--config", help="path to a custom config file", required=False
)

args = {
    key: value if value != "None" else None
    for key, value in parser.parse_args().__dict__.items()
}
# the result will have test_independent_varname as index name. This is not configurable.
args["result_independent_varname"] = args["test_independent_varname"]
